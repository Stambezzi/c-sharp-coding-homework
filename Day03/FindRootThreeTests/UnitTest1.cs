using findRootThree;
namespace FindRootThreeTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void WorksCorrectWithSmallNumber()
        {
            // Arrange
            int input = 1;
            int expected = 1;
            FindRootClass test = new FindRootClass();
            // Act
            int actual =test.FindRoot3(input);

            // Assert
            Assert.AreEqual(expected, actual);
        }
    }
}