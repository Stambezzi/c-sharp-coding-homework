﻿using findMissingNS;
namespace FindMissingTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
       
        public void WorksCorrectWithTenElements()
        {
            // Arrange
            int[] allNums = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };
            int[] input = { 2, 1, 5, 9, 7, 8, 4, 10, 6 };
            int expected = 3;
            FindMissingClass test = new FindMissingClass(allNums);

            // Act
            test.FindMissing(input);

            // Assert
            int actual = test.FindMissing(input);
            Assert.AreEqual(expected, actual);
        }
    }
}