using findMissingNS;
namespace FindMissingTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void WorksCorrectWithThreeElements()
        {
            // Arrange
            int[] allNums = { 1, 2, 3 };
            int[] input = { 3, 1 };
            int expected = 2;
            FindMissingClass test = new FindMissingClass(allNums);

            // Act
            test.FindMissing(input);

            // Assert
            int actual = test.FindMissing(input);
            Assert.AreEqual(expected, actual);
        }
    }
}