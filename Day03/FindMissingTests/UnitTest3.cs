﻿using findMissingNS;
namespace FindMissingTests
{
    [TestClass]
    public class UnitTest3
    {
        [TestMethod]

        public void WorksCorrectWithTenElements()
        {
            // Arrange
            int[] allNums = { };
            int[] input = { };
            int expected = -1;
            FindMissingClass test = new FindMissingClass(allNums);

            // Act
            test.FindMissing(input);

            // Assert
            int actual = test.FindMissing(input);
            Assert.AreEqual(expected, actual);
        }
    }
}