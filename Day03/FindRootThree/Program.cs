﻿namespace findRootThree
{
    public class FindRootClass
    {
        public int FindRoot3(int x)
        {
            int divider = 1;
            if (x == 0)
            {
                return 0;
            }
            while (divider < x)
            {
                if (x % divider == 0 && Math.Pow(divider, 3) == x)
                {
            Console.WriteLine(divider);
                    return divider;
                }
                divider++;
            }
            return divider;
        }
        public static void Main()
        {
            FindRootClass result = new FindRootClass();
            result.FindRoot3(0);
        }
    }
}
