using maxNum;
namespace maxNumTest
{
    [TestClass]
    public class maxNumTest
    {
        [TestMethod]
        public void PrintsCorretMaxNum()
        {
            // Arrange
            int coeficient = 5;
            int[] list = { 1, 4, 7, 9 };
            int expectedResult = 14;
            MaxNum output = new MaxNum();

            // Act
            output.printMaxNum(coeficient, list);

            // Assert
            int actual = output.printMaxNum(coeficient, list);
            Assert.AreEqual(expectedResult, actual, "result is not correct.");
           
        }
    }
}