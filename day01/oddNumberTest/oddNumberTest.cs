using oddNum;
namespace oddNumberTest
{
    [TestClass]
    public class oddNumberTest
    {
        [TestMethod]
        public void ChecksCorrectResult()
        {
            // Arrange
            int num = 5;
            OddNumber newNum = new OddNumber();
            // Act
            bool actualResult = newNum.isOdd(num);
            // Assert
            Assert.IsTrue(actualResult, $"the result is incorrect, {num} is odd" );
        }
    }
}