using smallTasks;

namespace stringReverseTest
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestReverseGivenFiveLetterWord()
        {
            // Arrange
            string input = "hello";
            string expected = "olleh";
            // Act
            string actual =  SmallTasks.Reverse(input);
            // Assert
            Assert.AreEqual(expected, actual);

        }
    }
}