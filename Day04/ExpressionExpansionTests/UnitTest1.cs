using exprExpNS;
namespace ExpressionExpansionTests
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void WorksCorrectWithOneTimeExpansion()
        {
            //Arrange
            string input = "1(aa)";
            string expected = "aa";
            ExpressionExpansion expression = new ExpressionExpansion();
            //Act
            string actual = expression.expand(input);
            //Assert
            Assert.AreEqual (expected, actual); 
        }
    }
}