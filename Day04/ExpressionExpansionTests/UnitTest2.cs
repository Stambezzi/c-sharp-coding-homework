﻿using exprExpNS;
namespace ExpressionExpansionTests
{
    [TestClass]
    public class UnitTest2
    {
        [TestMethod]
        public void WorksCorrectWithMultipleNestedExpansions()
        {
            //Arrange
            string input = "3(r2(i2(c)3(e)))";
            string expected = "ricceeeicceeericceeeicceeericceeeicceee";
            ExpressionExpansion expression = new ExpressionExpansion();
            //Act
            string actual = expression.expand(input);
            //Assert
            Assert.AreEqual(expected, actual);
        }
    }
}