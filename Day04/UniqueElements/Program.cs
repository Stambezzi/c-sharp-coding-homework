﻿using Microsoft.VisualBasic;
using System.Collections;

namespace uniqueElementsNS
{
    public class UniqueElementsClass
    {
        public static void Main()
        {
            UniqueElementsClass result = new UniqueElementsClass();
            List<int> myList = new List<int> { };
            result.uniqueElements(myList);
        }
        public List<int> uniqueElements(List<int> input)
        {
           HashSet<int> hash = input.Distinct().ToHashSet();
            List<int> list = hash.Distinct().ToList();
            return list;
        }
    }
}