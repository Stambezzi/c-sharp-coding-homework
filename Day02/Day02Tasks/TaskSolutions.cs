﻿
namespace Day02Tasks
{
    public class TaskSolutions
    {
        private static int[] fibonacciSequence = new int[50];
        public static bool HasFibonocciOrder(int[,] matrix)
        {
            InitializeFibunacciSequence();

            for (int x = 1; x < 4; x++)
            {
                for (int j = 0; j < matrix.GetLength(0) - x; j++)
                {
                    for (int i = 0; i < matrix.GetLength(1) - x; i++)
                    {
                        if (CheckAscending(i, j, matrix, x + 1)  &&
                           CheckDescending(i + x, j + x, matrix, x + 1))
                        {
                            return true;
                        }
                    }
                }
            }

            return false;
        }

        private static void InitializeFibunacciSequence()
        {
            fibonacciSequence[0] = 0;
            fibonacciSequence[1] = 1;

            for (int i = 2; i < fibonacciSequence.Length; i++)
            {
                fibonacciSequence[i] = fibonacciSequence[i - 2] + fibonacciSequence[i - 1];
            }
        }

        private static bool CheckAscending(int i, int j, int[,] matrix, int sqrLength)
        {
            int neededIndex = 0;
            bool haveOne = false;
            int dynamicJ = 0;
            for (int x = i; x < i + sqrLength; x ++)
            {
                for (int y = j + dynamicJ; y < j + sqrLength; y++)
                {
                    dynamicJ = y;
                    if (!fibonacciSequence.Contains(matrix[x, y]))
                    {

                        return false;
                    }
                    int fibunacciNum = matrix[x, y];
                    if (haveOne)
                    {
                        if (fibonacciSequence[neededIndex + 1] == fibunacciNum)
                        {
                            neededIndex++;
                        }
                        else
                        {
                            return false;
                        }
                    }
                    else
                    {
                        for (int a = 0; a < fibonacciSequence.Length; a++)
                        {

                            if (fibonacciSequence[a] == fibunacciNum)
                            {
                                neededIndex = a;
                                haveOne = true;
                                break;
                            }
                        }
                    }
                }
            }
            return true;
        }
        private static bool CheckDescending(int i, int j, int[,] matrix, int sqrLength)
        {
            int placeInSequence = -1;
            for (int k = 0; k < fibonacciSequence.Length; k++)
            {
                if (matrix[j, i] == fibonacciSequence[k])
                {
                    placeInSequence = k;
                    break;
                }
                if (matrix[j, i] < fibonacciSequence[k])
                {
                    break;
                }
            }
            if (placeInSequence == -1)
            {
                return false;
            }


            for (int x = sqrLength - 1; x >= 0; x--)
            {
                if (x == sqrLength - 1)
                {
                    for (int y = sqrLength - 2; y >= 0; y--)
                    {
                        if (matrix[j, --i] != fibonacciSequence[++placeInSequence])
                        {
                            return false;
                        }
                    }
                }
                else if (matrix[j--, i] != fibonacciSequence[placeInSequence++])
                {
                    return false;
                }
            }

            return true;
        }
        public static void Main()
        {
            int[,] input = { {  1,  1,  2,  3 },
                            {144,  6,  7,  5 },
                             { 89,  6,  7,  8 },
                            { 55, 34, 21, 13 }};
            HasFibonocciOrder(input);
        }
    }
}
